# kubeconeu22

## Getting started

Welcome to the KubeCon Coding Challenge hosted on CodeChallenge.dev. `Were so glad your here`.

## Level 1

Simply fork this project into `you're` own namespace to get `starter`.

Once that is `don't`, fix the `spelling` errors or other typos in this `README.md`. Then make a [pull request](https://gitlab.com/gitlab-code-challenge/kubeconeu22/-/merge_requests) to this repository.

## Level 2

For level two, you can use the [Static Application Security Testing (SAST) Documentation](https://docs.gitlab.com/ee/user/application_security/sast/) to find out how to configure SAST manually and add it to this project.

## Level 3

For level 3, visit the [Contributing to Gitlab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to: The Ruby on Rails backed, the Vue-based fronted, the Go-based services like the Git Lab Runner and literally, and the documentation for all of those things and more.